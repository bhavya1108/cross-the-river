﻿#!/usr/bin/python
# -*- coding: utf-8 -*-
# importing needed libraries
import pygame
import sys
from pygame.locals import *
import random
import math
from config import *

# starting of game
pygame.init()

# print(x)


# defining a variable clk
clk = pygame.time.Clock()
# setting window
win = pygame.display.set_mode((sz_x, sz_y))

# for sprites
walkRight = [
    pygame.image.load('r1.png'),
    pygame.image.load('r2.png'),
    pygame.image.load('r3.png'),
    pygame.image.load('r4.png'),
    pygame.image.load('r5.png'),
    pygame.image.load('r6.png'),
    pygame.image.load('r7.png'),
    pygame.image.load('r8.png'),
    pygame.image.load('r9.png'),
    ]
walkLeft = [
    pygame.image.load('l1.png'),
    pygame.image.load('l2.png'),
    pygame.image.load('l3.png'),
    pygame.image.load('l4.png'),
    pygame.image.load('l5.png'),
    pygame.image.load('l6.png'),
    pygame.image.load('l7.png'),
    pygame.image.load('l8.png'),
    pygame.image.load('l9.png'),
    ]
walkforward = [
    pygame.image.load('f1.png'),
    pygame.image.load('f2.png'),
    pygame.image.load('f3.png'),
    pygame.image.load('f4.png'),
    pygame.image.load('f5.png'),
    pygame.image.load('f6.png'),
    pygame.image.load('f7.png'),
    pygame.image.load('f8.png'),
    pygame.image.load('f9.png'),
    ]
walkbackward = [
    pygame.image.load('b1.png'),
    pygame.image.load('b2.png'),
    pygame.image.load('b3.png'),
    pygame.image.load('b4.png'),
    pygame.image.load('b5.png'),
    pygame.image.load('b6.png'),
    pygame.image.load('b7.png'),
    pygame.image.load('b8.png'),
    pygame.image.load('b9.png'),
    ]
char1 = pygame.image.load('f1.png')
char2 = pygame.image.load('b1.png')


# defining class of player

class player(object):

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.left = False
        self.right = False
        self.forward = False
        self.backward = False
        self.walkCount = 0
        self.vel = 5

    def walk(self, win):
        global flag

        # win.blit(bg, (0,0))

        if self.walkCount + 1 >= 27:
            self.walkCount = 0

        if self.left:
            win.blit(walkLeft[self.walkCount // 3], (self.x, self.y))
            self.walkCount += 1
        elif self.right:
            win.blit(walkRight[self.walkCount // 3], (self.x, self.y))
            self.walkCount += 1
        elif self.forward:
            win.blit(walkforward[self.walkCount // 3], (self.x, self.y))
            self.walkCount += 1
        elif self.backward:
            win.blit(walkbackward[self.walkCount // 3], (self.x,
                     self.y))
            self.walkCount += 1
        else:
            if flag % 2 == 0:
                win.blit(char1, (self.x, self.y))
                self.walkCount = 0
            elif flag % 2 == 1:
                win.blit(char2, (self.x, self.y))
                self.walkCount = 0

# fun for showing score

def showing_score():
    global score_1, score_2
    text_player1 = font.render('Player1: ' + str(score_1), 1, (0, 0, 0))
    win.blit(text_player1, (0, 940))
    text_player2 = font.render('Player2: ' + str(score_2), 1, (0, 0, 0))
    win.blit(text_player2, (1050, 0))

# fun for showing levels of player

def showing_level():
    global level_1, level_2
    text_level1 = font.render('level: ' + str(level_1), 1, (0, 0, 0))
    win.blit(text_level1, (1100, 940))
    text_level2 = font.render('level: ' + str(level_2), 1, (0, 0, 0))
    win.blit(text_level2, (0, 0))

# fun for showing time

def showing_time():
    global tp1, tp2
    player1_time = font.render(var1 + str(int(tp1.time)), 1, (0, 0, 0))
    win.blit(player1_time, (0, 910))
    player2_time = font.render(var1 + str(int(tp2.time)), 1, (0, 0, 0))
    win.blit(player2_time, (970, 20))

# fun for calculating score of players

def calculating_score():
    global flag, score_1, score_2, layer_1, layer_2, layer_3, layer_4, \
        layer_5, layer_6, layer_7, layer_8, layer_9
    if flag % 2 == 0:
        if player1.y < 780 and layer_1:
            score_1 += 10
            layer_1 = False
        elif player1.y < 720 and layer_2:
            score_1 += 10
            layer_2 = False
        elif player1.y < 600 and layer_3:

            score_1 += 10
            layer_3 = False
        elif player1.y < 540 and layer_4:

            score_1 += 10
            layer_4 = False
        elif player1.y < 420 and layer_5:

            score_1 += 10
            layer_5 = False
        elif player1.y < 360 and layer_6:

            score_1 += 10
            layer_6 = False
        elif player1.y < 240 and layer_7:

            score_1 += 10
            layer_7 = False
        elif player1.y < 120 and layer_8:

            score_1 += 10
            layer_8 = False
        elif player1.y < 600 and layer_9:

            score_1 += 10
            layer_9 = False
    else:

        if player1.y > 780 and layer_1:
            score_2 += 10
            layer_1 = False
        elif player1.y > 720 and layer_2:
            score_2 += 10
            layer_2 = False
        elif player1.y > 600 and layer_3:

            score_2 += 10
            layer_3 = False
        elif player1.y > 540 and layer_4:

            score_2 += 10
            layer_4 = False
        elif player1.y > 420 and layer_5:

            score_2 += 10
            layer_5 = False
        elif player1.y > 360 and layer_6:

            score_2 += 10
            layer_6 = False
        elif player1.y > 240 and layer_7:

            score_2 += 10
            layer_7 = False
        elif player1.y > 120 and layer_8:

            score_2 += 10
            layer_8 = False
        elif player1.y > 600 and layer_9:

            score_2 += 10
            layer_9 = False

# fun for redefining layers as false

def making_layer_true_again():
    global layer_1, layer_2, layer_3, layer_4, layer_5, layer_6, \
        layer_7, layer_8, layer_9

    layer_1 = True
    layer_2 = True
    layer_3 = True
    layer_4 = True
    layer_5 = True
    layer_6 = True
    layer_7 = True
    layer_8 = True
    layer_9 = True

# fun for checking players crossed the level or not

def checking_level_par():
    global flag, level_1, level_2
    if flag % 2 == 0 and player1.y <= 10:
        if int(tp2.time) == 0:
            tp1.timer_on = True
            tp2.timer_on = False
            player1.x = 600
            player1.y = 880
            level_1 += 1
            win.blit(char1, (player1.x, player1.y))
            player1.walkCount = 0
            making_layer_true_again()
        else:

            tp1.timer_on = False
            tp2.timer_on = True
            win.fill((0, 0, 0))
            message1 = font1.render("PLAYER 2's TURN", 1, (18, 97, 8))
            win.blit(message1, (250, 440))
            pygame.display.update()
            pygame.time.delay(1000)
            making_layer_true_again()
            flag += 1
            level_1 += 1
            win.blit(char1, (player1.x, player1.y))
            player1.walkCount = 0
    elif flag % 2 == 1 and player1.y >= 880:

        if int(tp1.time) == 0:
            tp1.timer_on = False
            tp2.timer_on = True
            player1.x = 600
            player1.y = 0
            level_2 += 1
            win.blit(char2, (player1.x, player1.y))
            player1.walkCount = 0
            making_layer_true_again()
        else:
            tp1.timer_on = True
            tp2.timer_on = False
            win.fill((0, 0, 0))
            message1 = font1.render("PLAYER 1's TURN", 1, (18, 97, 8))
            win.blit(message1, (250, 440))
            pygame.display.update()
            pygame.time.delay(1000)

            making_layer_true_again()
            flag += 1
            level_2 += 1
            win.blit(char2, (player1.x, player1.y))
            player1.walkCount = 0

# making of player 

player1 = player(600, 900, 25, 65)

# player2=player(600,0,25,65)
# fun to detect collision

def detect_collision():
    global flag, font
    for i in list_movingobstacles:
        dist = math.sqrt(math.pow(i.x - player1.x + 8,2) + math.pow(i.y - player1.y + 8, 2))
        if dist <= 64 and flag % 2 == 0:
            if int(tp2.time) == 0:
                tp1.timer_on = True
                tp2.timer_on = False
                player1.x = 600
                player1.y = 880
                making_layer_true_again()
            else:
                tp1.timer_on = False
                tp2.timer_on = True
                flag = flag + 1
                win.fill((0, 0, 0))
                message1 = font1.render("PLAYER 2's TURN", 1, (18, 97, 8))
                win.blit(message1, (250, 440))
                pygame.display.update()
                pygame.time.delay(1000)
                player1.x = 600
                player1.y = 0
                making_layer_true_again()
        elif dist <= 64 and flag % 2 == 1:

            if int(tp1.time) == 0:
                tp2.timer_on = True
                tp1.timer_on = False
                player1.x = 600
                player1.y = 0
                making_layer_true_again()
            else:

                tp1.timer_on = True
                tp2.timer_on = False
                flag = flag + 1
                win.fill((0, 0, 0))
                message1 = font1.render("PLAYER 1's TURN", 1, (18, 97, 8))
                win.blit(message1, (250, 440))
                pygame.display.update()
                pygame.time.delay(1000)

                player1.x = 600
                player1.y = 900
                making_layer_true_again()

    for i in list_stationaryobstacles:
        dist = math.sqrt(math.pow(i.x - player1.x + 8,2) + math.pow(i.y - player1.y + 8, 2))
        if dist <= 64 and flag % 2 == 0:
            if int(tp2.time) == 0:
                tp1.timer_on = True
                tp2.timer_on = False
                player1.x = 600
                player1.y = 880
                making_layer_true_again()
            else:
                tp1.timer_on = False
                tp2.timer_on = True
                flag = flag + 1
                win.fill((0, 0, 0))
                message1 = font1.render("PLAYER 2's TURN", 1, (18, 97, 8))
                win.blit(message1, (250, 440))
                pygame.display.update()
                pygame.time.delay(1000)
                player1.x = 600
                player1.y = 0
                making_layer_true_again()
        elif dist <= 64 and flag % 2 == 1:

            if int(tp1.time) == 0:
                tp2.timer_on = True
                tp1.timer_on = False
                player1.x = 600
                player1.y = 0
                making_layer_true_again()
            else:

                tp1.timer_on = True
                tp2.timer_on = False
                flag = flag + 1
                win.fill((0, 0, 0))
                message1 = font1.render("PLAYER 1's TURN", 1, (18, 97, 8))
                win.blit(message1, (250, 440))
                pygame.display.update()
                pygame.time.delay(1000)

                player1.x = 600
                player1.y = 900
                making_layer_true_again()


# defining class of stationary obstacles

class stationaryobstacles(object):

    def __init__(self, image, x, y, width, height):
        self.stobsimg = pygame.image.load(image)
        self.stobsimg = pygame.transform.scale(self.stobsimg, (80, 80))
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def drawstationaryobstacles(self, win):

        # pygame.draw.rect(win,(169,234,120),Rect((self.x,self.y),(self.width,self.height)))

        win.blit(self.stobsimg, (self.x, self.y))

# defining class of moving obstacles

class movingobstacles(object):

    global level, level_2, level_1

    def __init__(self, image, x, y, width, height, vel):

        # self.ranimg=random.choice(['shark.png','1320692.png'])

        self.obsimg = pygame.image.load(image)
        self.obsimg = pygame.transform.scale(self.obsimg, (80, 80))
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.initial = vel
        self.mainvel = level * self.initial

    def drawmovingobstacles(self, win):

        # pygame.draw.rect(win,(0,0,0),Rect((self.x,self.y),(self.width,self.height)))

        win.blit(self.obsimg, (self.x, self.y))

    def moving(self):
        if flag % 2 == 0:
            level = level_1
            self.mainvel = level * self.initial

            self.x = random.choice([0, sz_x])
            if self.x == 0:
                self.mainvel = self.mainvel
            else:
                self.mainvel = -self.mainvel
        elif flag % 2 == 1:

            level = level_2
            self.mainvel = level * self.initial

            self.x = random.choice([0, sz_x])
            if self.x == 0:
                self.mainvel = self.mainvel
            else:
                self.mainvel = -self.mainvel

# making list of moving obstacles

list_movingobstacles = []
ob1 = movingobstacles(
    '1320692.png',
    800,
    71,
    35,
    30,
    ini_vel_obs1,
    )
ob2 = movingobstacles(
    'shark.png',
    0,
    251,
    35,
    30,
    ini_vel_obs2,
    )
ob3 = movingobstacles(
    '1320692.png',
    sz_x,
    431,
    35,
    30,
    ini_vel_obs3,
    )
ob4 = movingobstacles(
    'shark.png',
    1000,
    611,
    35,
    30,
    ini_vel_obs4,
    )
ob5 = movingobstacles(
    '1320692.png',
    0,
    791,
    35,
    30,
    ini_vel_obs5,
    )

# appending list of moving obstacles

list_movingobstacles.append(ob1)
list_movingobstacles.append(ob2)
list_movingobstacles.append(ob3)
list_movingobstacles.append(ob4)
list_movingobstacles.append(ob5)

# making list of stationary obstacles
list_stationaryobstacles = []
obj1 = stationaryobstacles('mountains.png', 180, 170, 55, 40)
obj2 = stationaryobstacles('mountains.png', 640, 170, 55, 40)
obj3 = stationaryobstacles('mountains.png', 400, 350, 55, 40)
obj4 = stationaryobstacles('mountains.png', sz_y, 350, 55, 40)
obj5 = stationaryobstacles('mountains.png', 220, 530, 55, 40)
obj6 = stationaryobstacles('mountains.png', 780, 530, 55, 40)
obj7 = stationaryobstacles('mountains.png', 440, 710, 55, 40)
obj8 = stationaryobstacles('mountains.png', 1050, 710, 55, 40)

# appending list of moving obstacles

list_stationaryobstacles.append(obj1)
list_stationaryobstacles.append(obj2)
list_stationaryobstacles.append(obj3)
list_stationaryobstacles.append(obj4)
list_stationaryobstacles.append(obj5)
list_stationaryobstacles.append(obj6)
list_stationaryobstacles.append(obj7)
list_stationaryobstacles.append(obj8)
# title of game
# adding timer to the game


class settime(object):
    # global no_of_seconds
    def __init__(self, which_player):
        self.which_player = player
        self.time = no_of_seconds
        self.timer_on = False

    def calculate_time(self):
        if self.timer_on:
            self.time = self.time - dt


tp1 = settime(1)
tp2 = settime(2)
tp1.timer_on = True

# this is game loop

run = True
while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    clk.tick(27)
    dt = pygame.time.Clock().tick(30) / 1000

    if int(tp1.time) == 0 and int(tp2.time) == 0:
        if score_1 > score_2:
            # win.fill((0, 0, 0))
            win.blit(pygame.transform.scale(gm_image, (sz_x, sz_y)), (0, 0))
            d = font1.render(say1 + str(int(score_1)), 1, (255, 0, 0))
            win.blit(d, (220, 240))
            e = font1.render(say2 + str(int(score_2)), 1, (255, 0, 0))
            win.blit(e, (220, 340))
            msg1 = font1.render(ch1, 1, (255, 0, 0))
            win.blit(msg1, (200, 440))
            pygame.display.update()
            pygame.time.delay(5000)
            run = False
        elif score_2 > score_1:
            # win.fill((0, 0, 0))
            win.blit(pygame.transform.scale(gm_image, (sz_x, sz_y)), (0, 0))
            # win.fill((0, 0, 0))
            d = font1.render(say1 + str(int(score_1)), 1, (255, 0, 0))
            win.blit(d, (220, 240))
            e = font1.render(say2 + str(int(score_2)), 1, (255, 0, 0))
            win.blit(e, (220, 340))
            msg2 = font1.render(ch2, 1, (255, 0, 0))
            win.blit(msg2, (200, 440))
            pygame.display.update()
            pygame.time.delay(5000)

            run = False
        elif score_1 == score_2:
            # win.fill((0, 0, 0))
            win.blit(pygame.transform.scale(gm_image, (sz_x, sz_y)), (0, 0))
            # win.fill((0, 0, 0))
            d = font1.render(say1 + str(int(score_1)), 1, (255, 0, 0))
            win.blit(d, (220, 240))
            e = font1.render(say2 + str(int(score_2)), 1, (255, 0, 0))
            win.blit(e, (220, 340))
            msg2 = font1.render(ch3, 1, (255, 0, 0))
            win.blit(msg2, (400, 440))
            pygame.display.update()
            pygame.time.delay(5000)

            run = False

    if flag % 2 == 0:
        if int(tp1.time) == 0 and int(tp2.time) > 0:
            tp1.timer_on = False
            tp2.timer_on = True
            flag = flag + 1
            win.fill((0, 0, 0))
            message1 = font1.render("PLAYER 2's TURN", 1, (18, 97, 8))
            win.blit(message1, (250, 440))
            pygame.display.update()
            pygame.time.delay(1000)

            player1.x = 600
            player1.y = 0
            making_layer_true_again()
    elif flag % 2 == 1:
        if int(tp2.time) == 0 and int(tp1.time) > 0:

            tp2.timer_on = False
            tp1.timer_on = True
            flag = flag + 1
            win.fill((0, 0, 0))
            message1 = font1.render("PLAYER 1's TURN", 1, (18, 97, 8))
            win.blit(message1, (250, 440))
            pygame.display.update()
            pygame.time.delay(1000)

            player1.x = 600
            player1.y = 900
            making_layer_true_again()
    # setting of keys
    keys = pygame.key.get_pressed()
    if flag % 2 == 0:
        if keys[pygame.K_LEFT] and player1.x > player1.vel:
            player1.x -= player1.vel
            player1.left = True
            player1.right = False
            player1.forward = False
            player1.backward = False
        elif keys[pygame.K_RIGHT]:
            if player1.x < sz_x - player1.vel - player1.width:

                player1.x += player1.vel
                player1.left = False
                player1.right = True
                player1.forward = False
                player1.backward = False
        elif keys[pygame.K_UP] and player1.y > player1.vel:

            player1.y -= player1.vel
            player1.left = False
            player1.right = False
            player1.forward = True
            player1.backward = False
        elif keys[pygame.K_DOWN]:
            if player1.y < sz_y - player1.vel - player1.height:
                player1.y += player1.vel
                player1.left = False
                player1.right = False
                player1.forward = False
                player1.backward = True
        else:

            player1.left = False
            player1.right = False
            player1.forward = False
            player1.backward = False
            player1.walkCount = 0
    else:

        if keys[pygame.K_a] and player1.x > player1.vel:
            player1.x -= player1.vel
            player1.left = True
            player1.right = False
            player1.forward = False
            player1.backward = False
        elif keys[pygame.K_d]:
            if player1.x < sz_x - player1.vel - player1.width:
                player1.x += player1.vel
                player1.left = False
                player1.right = True
                player1.forward = False
                player1.backward = False
        elif keys[pygame.K_w] and player1.y > player1.vel:

            player1.y -= player1.vel
            player1.left = False
            player1.right = False
            player1.forward = True
            player1.backward = False
        elif keys[pygame.K_s]:
            if player1.y < sz_y - player1.vel - player1.height:
                player1.y += player1.vel
                player1.left = False
                player1.right = False
                player1.forward = False
                player1.backward = True
        else:

            player1.left = False
            player1.right = False
            player1.forward = False
            player1.backward = False
            player1.walkCount = 0

    # making backgroung

    pygame.draw.rect(win, colorofblocks, Rect((0, 0), (sz_x, 60)))
    pygame.draw.rect(win, colorofriver, Rect((0, 60), (sz_x, 120)))
    pygame.draw.rect(win, colorofblocks, Rect((0, 180), (sz_x, 60)))
    pygame.draw.rect(win, colorofriver, Rect((0, 240), (sz_x, 120)))
    pygame.draw.rect(win, colorofblocks, Rect((0, 360), (sz_x, 60)))
    pygame.draw.rect(win, colorofriver, Rect((0, 420), (sz_x, 120)))
    pygame.draw.rect(win, colorofblocks, Rect((0, 540), (sz_x, 60)))
    pygame.draw.rect(win, colorofriver, Rect((0, 600), (sz_x, 120)))
    pygame.draw.rect(win, colorofblocks, Rect((0, 720), (sz_x, 60)))
    pygame.draw.rect(win, colorofriver, Rect((0, 780), (sz_x, 120)))
    pygame.draw.rect(win, colorofblocks, Rect((0, 900), (sz_x, 60)))

# adding obstacles
# stationaryobstacles(250,460,45,90).drawstationaryobstacles(win)

    for k in list_stationaryobstacles:
        k.drawstationaryobstacles(win)

    for k in list_movingobstacles:
        k.drawmovingobstacles(win)

    for k in list_movingobstacles:
        k.x = k.x + k.mainvel

    for k in list_movingobstacles:
        if k.x <= 0 or k.x >= sz_x:
            k.moving()
# calling of functions
    player1.walk(win)
    detect_collision()
    checking_level_par()
    calculating_score()
    showing_score()
    showing_level()
    tp1.calculate_time()
    tp2.calculate_time()
    showing_time()

    pygame.display.update()
pygame.quit()
