ABOUT MY RIVER CROSSING CHALLENGE GAME :

1) Playing arena consists of a river with some partitions in it.

2) There are 2 players in the game, one at Top and another at Bottom (2 sides of the river bank).

3) A player is safe when it is standing on a partition/slab.

4) KEYS FOR PLAYER 1 IS: 
						FOR MOVING UPWARDS::UP ARROW KEY
						FOR MOVING DOWNWARDS::DOWN ARROW KEY
						FOR MOVING RIGHT::RIGHT ARROW KEY
						FOR MOVING LEFT::LEFT ARROW KEY

5) KEYS FOR PLAYER 2 IS: 
						FOR MOVING UPWARDS:: W
						FOR MOVING DOWNWARDS::S
						FOR MOVING RIGHT::D
						FOR MOVING LEFT::A

6) There are two kinds of obstacles, moving and fixed. The player starts from the
‘START’ partition and must reach the ‘END’ position for player 1 and the ‘END’ becomes ‘START’, ‘START’ becomes ‘END’ for Player 2.

7) The moving obstacles move from left to right or right to left and there can randomly start from any side.

8) The player can move up, down, left and right.

9) Player dies once he/she touches any obstacle.

10) As player crosses moving obstacle successfully, accrues 10 points and for
crossing fixed obstacles 5 points.

11) Only one player is playing at a given point of time.

12) Your aim is to make players reach the other end of the river.

13) The player wins the game based on the points scored in a fixed time interval(initialised to 45)..

14) A player continues to play till it's time gets over..

15) In every succesive level for each player speed of moving obstacles increases by (level * initial_pace).

				
				 I HOPE YOU WILL ENJOY MY GAME AND NEXT TIME I WILL TRY TO MAKE IT BETTER

ABOUT THE GAME CREATOR:

Name:- BHAVYA JAIN
College:- INTERNATIONAL INSTITUTE OF INFORMATION TECHNOLOGY
Branch:- CSE (B.tech,First Year)
Address:-GACHIBOWLI,HYDERABAD (TELENGANA)