import pygame
import random
pygame.init()
#title of the game
pygame.display.set_caption('my first game')
#image shown after game is over
gm_image=pygame.image.load('download.jpeg')
#font style 1
font1 = pygame.font.SysFont('comicsans', 100, True)
#font style 2
font = pygame.font.SysFont('comicsans', 30, True)
#no of seconds for which game should continue
no_of_seconds = 45

#adding messages to display at the end
ch1 = 'Player 1 wins the game'
ch2 = 'Player 2 wins the game'
ch3 = 'Match tied'

#adding initial velocity 
ini_vel_obs1 = 3
ini_vel_obs2 = 2
ini_vel_obs3 = 4.4
ini_vel_obs4 = 3.6
ini_vel_obs5 = 2.8

#adding colors of rivers and slabs
colorofriver = (32, 178, 170)
colorofblocks = (200, 200, 17)

#some global variables
flag = 0
score_1 = 0
score_2 = 0
layer_1 = True
layer_2 = True
layer_3 = True
layer_4 = True
layer_5 = True
layer_6 = True
layer_7 = True
layer_8 = True
layer_9 = True
#initial level
level_1 = 1
level_2 = 1
level = 1
#size of game window
sz_x = 1200
sz_y = 960

say1 = 'Player1 score:'
say2 = 'Player2 score:'

var1 = 'Time Remaining:'


